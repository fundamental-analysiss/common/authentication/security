package com.naga.share.market.fundamental.analysis.security.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.adaptor.security.AnalyzerDAO;
import com.naga.share.market.fundamental.analysis.adaptor.security.AuthenticationDAO;
import com.naga.share.market.fundamental.analysis.exception.NoDataException;
import com.naga.share.market.fundamental.analysis.model.security.entity.Analyzer;
import com.naga.share.market.fundamental.analysis.model.security.entity.Authentication;
import com.naga.share.market.fundamental.analysis.model.security.response.AnalyzerResponseDTO;
import com.naga.share.market.fundamental.analysis.security.constants.SecurityConstants;
import com.naga.share.market.fundamental.analysis.utils.ValidationUtils;
import com.naga.share.market.fundamental.analysis.utils.SecurityUtils;

import net.bytebuddy.utility.RandomString;

/**
 * @author Nagaaswin S
 *
 */
@Service
public class AuthenticationServiceImpl implements AuthenticationService {
	Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	AuthenticationDAO authenticationDAO;

	@Autowired
	AnalyzerDAO analyzerDAO;

	@Autowired
	MailService mailService;

	@Override
	public AnalyzerResponseDTO newAuthentication(String analyzerId, String emailId) {
		logger.info("AuthenticationService - newAuthentication - Start");
		AnalyzerResponseDTO analyzerResponse = new AnalyzerResponseDTO();
		Map<String, Object> inputParams = new HashMap<String, Object>();
		inputParams.put(SecurityConstants.ANALYZER_ID, analyzerId);
		inputParams.put(SecurityConstants.EMAIL_ID, emailId);
		inputParams.put(SecurityConstants.ANALYSER_RESPONSE, analyzerResponse);
		if (!ValidationUtils.isNotNull(analyzerDAO.findByAnalyzerId(analyzerId))) {
			analyzerResponse.setIsValidAnalyzerId(SecurityConstants.Y_INDICATOR);
			if (!ValidationUtils.isNotNull(analyzerDAO.findByEmailId(emailId))) {
				analyzerResponse.setIsValidEmailId(SecurityConstants.Y_INDICATOR);
				Authentication authentication = new Authentication();
				inputParams.put(SecurityConstants.AUTHENTICATION, authentication);
				generateAuthentication(inputParams);
			} else {
				analyzerResponse.setIsValidEmailId(SecurityConstants.N_INDICATOR);
			}

		} else {
			analyzerResponse.setIsValidAnalyzerId(SecurityConstants.N_INDICATOR);
		}
		logger.info("AuthenticationService - newAuthentication - End");
		return analyzerResponse;
	}

	@Override
	public AnalyzerResponseDTO validateAuthentication(Map<String, String> inputParams) {
		logger.info("AuthenticationService - validateAuthentication - Start");
		AnalyzerResponseDTO analyzerResponse = new AnalyzerResponseDTO();
		String analyzerId = inputParams.get(SecurityConstants.ANALYZER_ID);
		String emailId = inputParams.get(SecurityConstants.EMAIL_ID);
		String OTP = inputParams.get(SecurityConstants.OTP);
		Authentication authentication = authenticationDAO.findByAnalyzerId(analyzerId);
		if (ValidationUtils.isNotNull(authentication) && authentication.getAnalyzerId().equalsIgnoreCase(analyzerId)) {
			analyzerResponse.setIsValidAnalyzerId(SecurityConstants.Y_INDICATOR);
			if (authentication.getEmailId().equalsIgnoreCase(emailId)) {
				analyzerResponse.setIsValidEmailId(SecurityConstants.Y_INDICATOR);
				if (LocalTime.now().isBefore(authentication.getOneTimePassExpiration())
						&& authentication.getOneTimePassword().equalsIgnoreCase(OTP)) {
					analyzerResponse.setIsValidOneTimePassword(SecurityConstants.Y_INDICATOR);
				} else {
					analyzerResponse.setIsValidOneTimePassword(SecurityConstants.N_INDICATOR);
				}
			} else {
				analyzerResponse.setIsValidEmailId(SecurityConstants.N_INDICATOR);
			}
		} else {
			analyzerResponse.setIsValidAnalyzerId(SecurityConstants.N_INDICATOR);
		}
		logger.info("AuthenticationService - validateAuthentication - End");
		return analyzerResponse;
	}

	@Override
	public void removeAuthentication(String analyzerId) {
		logger.info("AuthenticationService - removeAuthentication - Start");
		authenticationDAO.deleteByAnalyzerId(analyzerId);
		logger.info("AuthenticationService - removeAuthentication - End");
	}

	@Override
	public AnalyzerResponseDTO generateAuthForForgotPassword(String emailId, String DOB) {
		logger.info("AuthenticationService - generateAuthForForgotPassword - Start");
		AnalyzerResponseDTO analyzerResponse = new AnalyzerResponseDTO();
		Map<String, Object> inputParams = new HashMap<String, Object>();
		LocalDate dateOfBirth = SecurityUtils.DOBDateConversion(DOB);
		try {
			Analyzer analyzer = analyzerDAO.findByEmailIdAndDOB(emailId, dateOfBirth);
			Authentication authentication = new Authentication();
			analyzerResponse.setIsValidEmailId(SecurityConstants.Y_INDICATOR);
			analyzerResponse.setIsValidDOB(SecurityConstants.Y_INDICATOR);
			inputParams.put(SecurityConstants.ANALYZER_ID, analyzer.getAnalyzerId());
			inputParams.put(SecurityConstants.EMAIL_ID, analyzer.getEmailId());
			inputParams.put(SecurityConstants.ANALYSER_RESPONSE, analyzerResponse);
			inputParams.put(SecurityConstants.AUTHENTICATION, authentication);
			generateAuthentication(inputParams);
			analyzerDAO.updateAnalyzer(analyzer);
		} catch (NoDataException e) {
			logger.error("No corresponding data for given email ID and date of birth");
			analyzerResponse.setIsValidEmailId(SecurityConstants.N_INDICATOR);
			analyzerResponse.setIsValidDOB(SecurityConstants.N_INDICATOR);
			analyzerResponse.setIsOneTimePasswordGenerated(SecurityConstants.N_INDICATOR);
		}
		logger.info("AuthenticationService - generateAuthForForgotPassword - End");
		return analyzerResponse;
	}

	private void generateAuthentication(Map<String, Object> inputParams) {
		logger.info("AuthenticationService - generateAuthentication - Start");
		String analyzerId = (String) inputParams.get(SecurityConstants.ANALYZER_ID);
		String emailId = (String) inputParams.get(SecurityConstants.EMAIL_ID);
		Authentication authentication = (Authentication) inputParams.get(SecurityConstants.AUTHENTICATION);
		AnalyzerResponseDTO analyzerResponse = (AnalyzerResponseDTO) inputParams
				.get(SecurityConstants.ANALYSER_RESPONSE);
		String OTP = RandomString.make(7);
		authentication.setAnalyzerId(analyzerId);
		authentication.setEmailId(emailId);
		authentication.setOneTimePassword(OTP);
		authentication.setOneTimePassExpiration(SecurityUtils.setTimeExpiration());
		logger.debug(authentication.getAnalyzerId());
		try {
			authenticationDAO.insertAuthentication(authentication);
			analyzerResponse.setIsOneTimePasswordGenerated(SecurityConstants.Y_INDICATOR);
		} catch (Exception e) {
			logger.error("AuthenticationService - newAuthentication - OTP is Already generated for analyser ID: {}",
					analyzerId);
			analyzerResponse.setIsOneTimePasswordGenerated(SecurityConstants.N_INDICATOR);
		}
		if (analyzerResponse.getIsOneTimePasswordGenerated().equalsIgnoreCase(SecurityConstants.Y_INDICATOR)) {
			try {
				String mailSubject = SecurityConstants.AUTH_MAIL_SUBJECT;
				String mailBody = SecurityConstants.MAIL_HEADER + SecurityConstants.SINGLE_QUOTE + OTP
						+ SecurityConstants.SINGLE_QUOTE + SecurityConstants.AUTH_MAIL_BODY_OTP
						+ SecurityConstants.SINGLE_QUOTE + analyzerId + SecurityConstants.SINGLE_QUOTE
						+ SecurityConstants.MAIL_BODY_DATETIME + ValidationUtils.getSysDateTime()
						+ SecurityConstants.AUTH_MAIL_BODY_CONCLUSION + SecurityConstants.MAIL_FOOTER;
				logger.debug("Email: {} , Subject: {} , Body: {}", emailId, mailSubject, mailBody);
				mailService.sendTextMail(emailId, mailSubject, mailBody);
				analyzerResponse.setIsMailSent(SecurityConstants.Y_INDICATOR);
			} catch (Exception e) {
				logger.error("Mail not sent issue.");
				analyzerResponse.setIsMailSent(SecurityConstants.N_INDICATOR);
			}

		}
		logger.info("AuthenticationService - generateAuthentication - End");
	}
}
