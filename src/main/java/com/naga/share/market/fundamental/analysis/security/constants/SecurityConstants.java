package com.naga.share.market.fundamental.analysis.security.constants;

public class SecurityConstants {

	private SecurityConstants() {
	}

//	#INPUT
	public static final String ANALYZER_ID = "analyzerId";
	public static final String PASSWORD = "password";
	public static final String FIRSTNAME = "firstName";
	public static final String LASTNAME = "lastName";
	public static final String EMAIL_ID = "emailId";
	public static final String DOB = "dateOfBirth";
	public static final String OTP = "oneTimePassword";
	public static final String SYSTEM = "system";
	public static final String ANALYSER_RESPONSE = "analyserResponse";
	public static final String AUTHENTICATION = "authentication";
	public static final String Y_INDICATOR = "Y";
	public static final String N_INDICATOR = "N";

//	#ROLES
	public static final String MAINTAINER = "MAINTAINER";
	public static final String ANALYZER = "ANALYZER";

//	#MAIL
	public static final String MAIL_HEADER = "Dear Analyzer,\n\n";
	public static final String MAIL_FOOTER = "In case you have any queries / clarifications, please call us at :\n\n"
			+ "9962654112\n\n" + "or mail us at\n\n" + "nagaaswin775@gmail.com\n\n\n" + "Sincerely,\n" + "Nagaaswin S";
	public static final String AUTH_MAIL_SUBJECT = "One Time Password";
	public static final String AUTH_MAIL_BODY_OTP = " is SECRET OTP for your Analyzer ID : ";
	public static final String MAIL_BODY_DATETIME = " on ";
	public static final String AUTH_MAIL_BODY_CONCLUSION = " . OTP valid for 5 mins. Pls do not share OTP with anyone.\n\n";
	public static final String FORGOT_MAIL_SUBJECT = "Confidential";
	public static final String FORGOT_MAIL_BODY_ANALYZER_ID = "Your Analyzer ID: ";
	public static final String FORGOT_MAIL_BODY_PASSWORD = " and your password: ";
	public static final String FORGOT_MAIL_BODY_CONCLUSION = ". Once logged in pls update your password. Pls do not share password with anyone.\n\n";
	public static final String SINGLE_QUOTE = "'";
	public static final String SET_FROM = "Fundamental Analysis";
}
