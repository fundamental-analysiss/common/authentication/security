package com.naga.share.market.fundamental.analysis.security.access;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.security.access.prepost.PreAuthorize;

import com.naga.share.market.fundamental.analysis.security.constants.SecurityConstants;

/**
 * @author Nagaaswin S
 *
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@PreAuthorize("hasRole('" + SecurityConstants.MAINTAINER + "')")
public @interface IsMaintainer {
}
