package com.naga.share.market.fundamental.analysis.security.service;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.exception.NoDataException;
import com.naga.share.market.fundamental.analysis.model.security.response.AnalyzerResponseDTO;
import com.naga.share.market.fundamental.analysis.model.security.response.AllAnalyzersResponseDTO;

/**
 * @author Nagaaswin S
 *
 */
@Service
public interface AnalyzerService {

	/**
	 * Service method to add new Analyzer
	 * 
	 * @param inputParams
	 * @return AnalyzerResponse
	 */
	public AnalyzerResponseDTO addAnalyzer(Map<String, String> inputParams);

	/**
	 * 
	 * Service Method to generate new password for analyzer after Authentication
	 * 
	 * @param inputParams
	 * @return AnalyzerResponse
	 * @throws NoDataException
	 */
	public AnalyzerResponseDTO forgotAnalyzer(Map<String, String> inputParams) throws NoDataException;

	/**
	 * Service Method to update Analyzer details
	 * 
	 * @param inputParams
	 * @throws NoDataException
	 */
	public AnalyzerResponseDTO updateAnalyzer(Map<String, String> inputParams) throws NoDataException;

	/**
	 * 
	 * Service method to add higher role to a Analyzer
	 * 
	 * @param masterAnalyzerId
	 * @param analyzerId
	 * @return boolean
	 * @throws NoDataException
	 */
	public AnalyzerResponseDTO updateAnalyzerRole(String masterAnalyzerId, String analyzerId) throws NoDataException;

	/**
	 * Service Method to get all analyzer details
	 *
	 * @param analyzerId
	 * @return GetAllAnalyzers
	 */
	public AllAnalyzersResponseDTO getAllAnalyzer(String analyzerId);

	/**
	 * Service Method to delete Analyzer details
	 * 
	 * @param inputParams
	 * @throws NoDataException
	 */
	public AnalyzerResponseDTO deleteAnalyzer(String masterAnalyzerId, String analyzerId) throws NoDataException;
}
