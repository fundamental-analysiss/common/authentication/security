package com.naga.share.market.fundamental.analysis.security.config;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.naga.share.market.fundamental.analysis.security.service.AnalyzerDetailsService;

/**
 * @author Nagaaswin S
 *
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(jsr250Enabled = true, prePostEnabled = true, proxyTargetClass = true)
public class AppSecurityConfig extends WebSecurityConfigurerAdapter {

	Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	AnalyzerDetailsService analyzerDetailsService;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		logger.info("AppSecurityConfig  - configure AuthenticationManagerBuilder method - start");
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(analyzerDetailsService);
		authProvider.setPasswordEncoder(passwordEncoder());
		auth.authenticationProvider(authProvider);
		logger.info("AppSecurityConfig  - configure AuthenticationManagerBuilder method - end");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		logger.info("AppSecurityConfig  - configure HttpSecurity method - start");
		http.cors().disable().csrf().disable().authorizeRequests().antMatchers("/authentication/**").permitAll()
				.antMatchers("/analyzer/**").permitAll().anyRequest().authenticated().and().formLogin()
				.loginPage("/Login").loginProcessingUrl("/authenticateTheAnalyzer").defaultSuccessUrl("/", true)
				.permitAll().and().logout().permitAll();
		logger.info("AppSecurityConfig  - configure HttpSecurity method - end");
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

}
