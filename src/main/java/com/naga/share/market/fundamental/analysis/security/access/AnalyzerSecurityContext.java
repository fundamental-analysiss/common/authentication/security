package com.naga.share.market.fundamental.analysis.security.access;

import java.security.Principal;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * @author Nagaaswin S
 *
 */
@Component
public class AnalyzerSecurityContext {

	public String getAnalyzerId() {
		return SecurityContextHolder.getContext().getAuthentication().getName();
	}

	public Authentication getAnalyzerAuth() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	public SecurityContext getContext() {
		return SecurityContextHolder.getContext();
	}

	public Principal getPrincipal() {
		return (Principal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

}
