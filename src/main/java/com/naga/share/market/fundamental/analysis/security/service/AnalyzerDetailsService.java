package com.naga.share.market.fundamental.analysis.security.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.adaptor.constants.AdaptorConstants;
import com.naga.share.market.fundamental.analysis.adaptor.security.AnalyzerDAO;
import com.naga.share.market.fundamental.analysis.exception.NoDataException;
import com.naga.share.market.fundamental.analysis.model.security.entity.Analyzer;
import com.naga.share.market.fundamental.analysis.utils.SecurityUtils;
import com.naga.share.market.fundamental.analysis.utils.ValidationUtils;

/**
 * @author Nagaaswin S
 *
 */
@Service
public class AnalyzerDetailsService implements UserDetailsService {

	Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	private AnalyzerDAO analyzerDAO;

	@Override
	public UserDetails loadUserByUsername(String analyzerName) {
		logger.info("AnalyzerDetailsService - loadUserByUsername Method - start");
		Analyzer analyzer = null;
		try {
			analyzer = analyzerDAO.findByAnalyzerId(analyzerName);
			if (!ValidationUtils.isNotNull(analyzer)) {
				throw new NoDataException(AdaptorConstants.SECTOR_NOT_FOUND);
			}
		} catch (NoDataException e) {
			e.printStackTrace();
		}
		boolean accountExpired = SecurityUtils.expirationCheck(analyzer.getAccountExpiration());
		boolean credentialExpired = SecurityUtils.expirationCheck(analyzer.getCredentialExpiration());
		UserDetails user = User.withUsername(analyzer.getAnalyzerId()).password(analyzer.getPassword())
				.disabled(analyzer.isDisabled())
				.roles(analyzer.getRoles().toArray(new String[analyzer.getRoles().size()]))
				.accountExpired(accountExpired).credentialsExpired(credentialExpired).accountLocked(analyzer.isLocked())
				.build();
		logger.debug("AnalyzerDetailsService - Analyzer credentials - {} ", user.getUsername());
		logger.info("AnalyzerDetailsService - loadUserByUsername Method - end");
		return user;
	}
}
