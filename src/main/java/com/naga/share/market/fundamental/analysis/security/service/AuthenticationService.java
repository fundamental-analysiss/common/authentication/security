package com.naga.share.market.fundamental.analysis.security.service;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.model.security.response.AnalyzerResponseDTO;

/**
 * @author Nagaaswin S
 *
 */
@Service
public interface AuthenticationService {

	/**
	 * Service method to generate authentication for new Analyzer
	 * 
	 * @param analyzerId
	 * @param emailId
	 * @return AnalyzerResponse
	 */
	public AnalyzerResponseDTO newAuthentication(String analyzerId, String emailId);

	/**
	 * Service method to validate the authentication
	 * 
	 * @param inputParams
	 * @return AnalyzerResponse
	 */
	public AnalyzerResponseDTO validateAuthentication(Map<String, String> inputParams);

	/**
	 * Service Method to remove authentication after validation
	 * 
	 * @param analyzerId
	 */
	public void removeAuthentication(String analyzerId);

	/**
	 * Service method to generate the authentication for analyzers who forgot
	 * password
	 * 
	 * @param emailId
	 * @param DOB
	 * @return AnalyzerResponse
	 */
	public AnalyzerResponseDTO generateAuthForForgotPassword(String emailId, String DOB);
}
