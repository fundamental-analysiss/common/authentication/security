package com.naga.share.market.fundamental.analysis.security.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.adaptor.security.AnalyzerDAO;
import com.naga.share.market.fundamental.analysis.exception.NoDataException;
import com.naga.share.market.fundamental.analysis.model.security.entity.Analyzer;
import com.naga.share.market.fundamental.analysis.model.security.response.AllAnalyzers;
import com.naga.share.market.fundamental.analysis.model.security.response.AnalyzerResponseDTO;
import com.naga.share.market.fundamental.analysis.model.security.response.AllAnalyzersResponseDTO;
import com.naga.share.market.fundamental.analysis.security.constants.SecurityConstants;
import com.naga.share.market.fundamental.analysis.utils.ValidationUtils;
import com.naga.share.market.fundamental.analysis.utils.SecurityUtils;

import net.bytebuddy.utility.RandomString;

/**
 * @author Nagaaswin S
 *
 */
@Service
public class AnalyzerServiceImpl implements AnalyzerService {
	Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	AnalyzerDAO analyzerDAO;

	@Autowired
	AuthenticationService authenticationService;

	@Autowired
	MailService mailService;

	@Override
	public AnalyzerResponseDTO addAnalyzer(Map<String, String> inputParams) {
		logger.info("AnalyzerService - addAnalyzer method start");
		String analyzerId = inputParams.get(SecurityConstants.ANALYZER_ID);
		AnalyzerResponseDTO analyzerResponse = authenticationService.validateAuthentication(inputParams);
		try {
			if (analyzerResponse.getIsValidOneTimePassword().equalsIgnoreCase(SecurityConstants.Y_INDICATOR)) {
				Analyzer analyzer = new Analyzer();
				analyzer.setAnalyzerId(analyzerId);
				analyzer.setEmailId(inputParams.get(SecurityConstants.EMAIL_ID));
				analyzer.setPassword(SecurityUtils.passwordEncoder(inputParams.get(SecurityConstants.PASSWORD)));
				analyzer.setFirstName(inputParams.get(SecurityConstants.FIRSTNAME));
				analyzer.setLastName(inputParams.get(SecurityConstants.LASTNAME));
				analyzer.setDateOfBirth(SecurityUtils.DOBDateConversion(inputParams.get(SecurityConstants.DOB)));
				analyzer.setDisabled(false);
				analyzer.setRoles(assignRoles());
				analyzer.setAccountExpiration(SecurityUtils.setDateExpiration());
				analyzer.setCredentialExpiration(SecurityUtils.setDateExpiration());
				analyzer.setLocked(false);
				analyzer.setLastUpdatedBy(SecurityConstants.SYSTEM);
				logger.debug(analyzer.getAnalyzerId());
				analyzerDAO.insertAnalyzer(analyzer);
				analyzerResponse.setIsAnalyzerAdded(SecurityConstants.Y_INDICATOR);
			} else {
				analyzerResponse.setIsAnalyzerAdded(SecurityConstants.N_INDICATOR);
			}
		} finally {
			authenticationService.removeAuthentication(analyzerId);
		}
		logger.info("AnalyzerService - addAnalyzer method end");
		return analyzerResponse;
	}

	@Override
	public AnalyzerResponseDTO forgotAnalyzer(Map<String, String> inputParams) throws NoDataException {
		logger.info("AnalyzerService - forgotAnalyzer method start");
		String analyzerId = inputParams.get(SecurityConstants.ANALYZER_ID);
		AnalyzerResponseDTO analyzerResponse = authenticationService.validateAuthentication(inputParams);
		try {
			if (analyzerResponse.getIsValidOneTimePassword().equalsIgnoreCase(SecurityConstants.Y_INDICATOR)) {
				Analyzer analyzer = analyzerDAO.findByAnalyzerId(analyzerId);
				if (ValidationUtils.isNotNull(analyzer)) {
					analyzerResponse.setIsValidAnalyzerId(SecurityConstants.Y_INDICATOR);
					LocalDate DOB = SecurityUtils.DOBDateConversion(inputParams.get(SecurityConstants.DOB));
					if (analyzer.getDateOfBirth().equals(DOB)) {
						analyzerResponse.setIsValidDOB(SecurityConstants.Y_INDICATOR);
						String password = RandomString.make(10);
						analyzer.setPassword(SecurityUtils.passwordEncoder(password));
						analyzer.setLastUpdatedBy(SecurityConstants.SYSTEM);
						analyzerDAO.updateAnalyzer(analyzer);
						try {
							String emailId = analyzer.getEmailId();
							String mailSubject = SecurityConstants.FORGOT_MAIL_SUBJECT;
							String mailBody = SecurityConstants.MAIL_HEADER
									+ SecurityConstants.FORGOT_MAIL_BODY_ANALYZER_ID + SecurityConstants.SINGLE_QUOTE
									+ analyzerId + SecurityConstants.SINGLE_QUOTE
									+ SecurityConstants.FORGOT_MAIL_BODY_PASSWORD + SecurityConstants.SINGLE_QUOTE
									+ password + SecurityConstants.SINGLE_QUOTE + SecurityConstants.MAIL_BODY_DATETIME
									+ ValidationUtils.getSysDateTime() + SecurityConstants.FORGOT_MAIL_BODY_CONCLUSION
									+ SecurityConstants.MAIL_FOOTER;
							logger.debug("AnalyzerService - forgotAnalyzer - Email: {} , Subject: {} , Body: {}",
									emailId, mailSubject, mailBody);
							mailService.sendTextMail(emailId, mailSubject, mailBody);
							analyzerResponse.setIsMailSent(SecurityConstants.Y_INDICATOR);
						} catch (Exception e) {
							logger.error("Error during sending mail for analyzer id :{}", analyzerId);
							analyzerResponse.setIsMailSent(SecurityConstants.N_INDICATOR);
						}
						analyzerResponse.setIsNewPasswordGenerated(SecurityConstants.Y_INDICATOR);
					} else {
						analyzerResponse.setIsValidDOB(SecurityConstants.N_INDICATOR);
					}
				} else {
					analyzerResponse.setIsValidAnalyzerId(SecurityConstants.N_INDICATOR);
				}
			} else {
				analyzerResponse.setIsNewPasswordGenerated(SecurityConstants.N_INDICATOR);
			}
		} finally {
			authenticationService.removeAuthentication(analyzerId);
		}
		logger.info("AnalyzerService - forgotAnalyzer method end");
		return analyzerResponse;
	}

	@Override
	public AnalyzerResponseDTO updateAnalyzer(Map<String, String> inputParams) throws NoDataException {
		logger.info("AnalyzerService - updateAnalyzer method - start");
		AnalyzerResponseDTO analyzerResponse = new AnalyzerResponseDTO();
		String analyzerId = inputParams.get(SecurityConstants.ANALYZER_ID);
		Analyzer analyzer = analyzerDAO.findByAnalyzerId(analyzerId);
		if (ValidationUtils.isNotNull(analyzer)) {
			analyzerResponse.setIsValidAnalyzerId(SecurityConstants.Y_INDICATOR);
			if (inputParams.containsKey(SecurityConstants.EMAIL_ID)) {
				String emailId = inputParams.get(SecurityConstants.EMAIL_ID);
				if (!ValidationUtils.isNotNull(analyzerDAO.findByEmailId(emailId))) {
					analyzerResponse.setIsValidEmailId(SecurityConstants.Y_INDICATOR);
					analyzer.setEmailId(emailId);
				} else {
					logger.error("Entered email id is already been in use");
					analyzerResponse.setIsValidEmailId(SecurityConstants.N_INDICATOR);
					return analyzerResponse;
				}
			}
			if (inputParams.containsKey(SecurityConstants.PASSWORD)) {
				analyzer.setPassword(SecurityUtils.passwordEncoder(inputParams.get(SecurityConstants.PASSWORD)));
			}
			if (inputParams.containsKey(SecurityConstants.FIRSTNAME)) {
				analyzer.setFirstName(inputParams.get(SecurityConstants.FIRSTNAME));
			}
			if (inputParams.containsKey(SecurityConstants.LASTNAME)) {
				analyzer.setLastName(inputParams.get(SecurityConstants.LASTNAME));
			}
			if (inputParams.containsKey(SecurityConstants.DOB)) {
				analyzer.setDateOfBirth(SecurityUtils.DOBDateConversion(inputParams.get(SecurityConstants.DOB)));
			}
			analyzer.setLastUpdatedBy(analyzerId);
			logger.debug("AnalyzerService- updateAnalyzer : {}", analyzer.getAnalyzerId());
			try {
				analyzerDAO.updateAnalyzer(analyzer);
				analyzerResponse.setIsAnalyzerUpdated(SecurityConstants.Y_INDICATOR);
			} catch (Exception exception) {
				exception.printStackTrace();
				analyzerResponse.setIsAnalyzerUpdated(SecurityConstants.N_INDICATOR);
			}
		} else

		{
			analyzerResponse.setIsValidAnalyzerId(SecurityConstants.N_INDICATOR);
			analyzerResponse.setIsValidEmailId(SecurityConstants.N_INDICATOR);
			analyzerResponse.setIsAnalyzerUpdated(SecurityConstants.N_INDICATOR);
		}
		logger.info("AnalyzerService - updateAnalyzer method - end");
		return analyzerResponse;
	}

	@Override
	public AnalyzerResponseDTO updateAnalyzerRole(String masterAnalyzerId, String analyzerId) throws NoDataException {
		logger.info("AnalyzerService - updateAnalyzerRole method - start");
		AnalyzerResponseDTO analyzerResponse = new AnalyzerResponseDTO();
		Analyzer analyzer = analyzerDAO.findByAnalyzerId(analyzerId);
		if (ValidationUtils.isNotNull(analyzer)) {
			analyzerResponse.setIsValidAnalyzerId(SecurityConstants.Y_INDICATOR);
			analyzer.getRoles().add(SecurityConstants.MAINTAINER);
			analyzer.setLastUpdatedBy(masterAnalyzerId);
			try {
				analyzerDAO.updateAnalyzer(analyzer);
				analyzerResponse.setIsAnalyzerRoleUpdated(SecurityConstants.Y_INDICATOR);
			} catch (Exception exception) {
				exception.printStackTrace();
				logger.error("Cannot update analyzer role data");
				analyzerResponse.setIsAnalyzerRoleUpdated(SecurityConstants.N_INDICATOR);
			}
		} else {
			analyzerResponse.setIsValidAnalyzerId(SecurityConstants.N_INDICATOR);
		}
		logger.info("AnalyzerService - updateAnalyzerRole method - end");
		return analyzerResponse;
	}

	@Override
	public AllAnalyzersResponseDTO getAllAnalyzer(String analyzerId) {
		logger.info("AnalyzerService - getAllAnalyzer method - end");
		AllAnalyzersResponseDTO getAllAnalyzers = new AllAnalyzersResponseDTO();
		List<AllAnalyzers> allAnalyzers = new ArrayList<AllAnalyzers>();
		List<Analyzer> analyzers = analyzerDAO.findAll();
		for (Analyzer analyzer : analyzers) {
			AllAnalyzers analyzerDetails = new AllAnalyzers();
			analyzerDetails.setAnalyzerId(analyzer.getAnalyzerId());
			analyzerDetails.setFirstName(analyzer.getFirstName());
			analyzerDetails.setLastName(analyzer.getLastName());
			analyzerDetails.setRoles(analyzer.getRoles());
			allAnalyzers.add(analyzerDetails);
		}
		getAllAnalyzers.setAllAnalyzers(allAnalyzers);
		logger.info("AnalyzerService - getAllAnalyzer method - end");
		return getAllAnalyzers;
	}

	private ArrayList<String> assignRoles() {
		ArrayList<String> roles = new ArrayList<String>();
		roles.add(SecurityConstants.ANALYZER);
		return roles;
	}

	@Override
	public AnalyzerResponseDTO deleteAnalyzer(String masterAnalyzerId, String analyzerId) throws NoDataException {
		logger.info("AnalyzerService - deleteAnalyzer method - start");
		AnalyzerResponseDTO analyzerResponse = new AnalyzerResponseDTO();
		Analyzer analyzer = analyzerDAO.findByAnalyzerId(analyzerId);
		if (ValidationUtils.isNotNull(analyzer)) {
			analyzerResponse.setIsValidAnalyzerId(SecurityConstants.Y_INDICATOR);
			try {
				analyzerDAO.deleteByAnalyzerId(analyzer.getAnalyzerId());
				analyzerResponse.setIsAnalyzerDeleted(SecurityConstants.Y_INDICATOR);
			} catch (Exception exception) {
				exception.printStackTrace();
				logger.error("Cannot delete the analyzer with id :{}", analyzerId);
				analyzerResponse.setIsAnalyzerDeleted(SecurityConstants.N_INDICATOR);
			}
		} else {
			analyzerResponse.setIsValidAnalyzerId(SecurityConstants.N_INDICATOR);
		}
		logger.info("AnalyzerService - deleteAnalyzer method - end");
		return analyzerResponse;
	}

}
