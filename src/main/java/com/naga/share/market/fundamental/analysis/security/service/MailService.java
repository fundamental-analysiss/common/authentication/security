package com.naga.share.market.fundamental.analysis.security.service;

import org.springframework.stereotype.Service;

/**
 * @author Nagaaswin S
 *
 */
@Service
public interface MailService {

	/**
	 *
	 * A method to send normal Text Mail
	 *
	 * @param to
	 * @param subject
	 * @param text
	 */
	public void sendTextMail(String to, String subject, String text);
}
