package com.naga.share.market.fundamental.analysis.security.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import com.naga.share.market.fundamental.analysis.security.constants.SecurityConstants;

/**
 * @author Nagaaswin S
 *
 */
@Service
public class MailServiceImpl implements MailService {
	Logger logger = LogManager.getLogger(this.getClass());
	@Autowired
	private JavaMailSender emailSender;

	@Override
	public void sendTextMail(String to, String subject, String body) {
		logger.info("MailService - sendTextMail method - start");
		MimeMessagePreparator preparator = mimeMessage -> {
			MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
			message.setTo(to);
			message.setFrom("noreply@sharemarketanalysis.herokuapp.com", SecurityConstants.SET_FROM);
			message.setSubject(subject);
			message.setText(body, false);
			message.setPriority(1);
			logger.debug("Mailservice - Mail - From :{}, To: {}, Subject: {}, Body: {}", SecurityConstants.SET_FROM, to,
					subject, body);
		};
		emailSender.send(preparator);
		logger.info("MailService  - sendTextMail method - end");
	}

}
